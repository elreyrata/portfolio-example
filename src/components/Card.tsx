import React from 'react';
import styled from 'styled-components';

const StyledStack = styled.span`
    display: flex;
    flex-direction: row;
    gap: 15px;

    .circle {
        display: flex;
        width: 50px;
        height: 50px;
        border-radius: 200em;
    }
`

const StyledCard = styled.span`
    display: flex;
    flex-direction: column;
    padding: 25px;
    max-width: 100%;
    min-width: max-content;

    max-height: min-content;

    * {
        margin: 0;
    }

    background-color: var(--brighter);

    h3 {
        border-bottom: var(--dark) solid 1px;
    }

    h4 {
        margin-bottom: 10px;
    }

    .stack {
        width: calc(33% - 50px);
        min-width: max-content;
    }
`

const StyledCardBody = styled.span`
    display: flex;
    flex-direction: row;
    margin-top: 10px;

    gap: 50px;
`

interface CardProps {
    position: string;
    company: string;
    description: string;
    assets?: any[];
}

export const Card: React.FC<CardProps> = ({ position, company, description, assets }) => (
    <StyledCard>
        <h3>{`${position} [ ${company} ]`}</h3>
        <StyledCardBody>
            <span className="stack">
                <h4>Stack</h4>
                {assets ? <StyledStack>
                    { assets.map((asset, index) => <img src={asset} className='circle' alt='asset' key={index}/>) }
                </StyledStack> : null}
            </span>
            <span className="description">
                <h4>Description</h4>
                <p>{description}</p>
            </span>
        </StyledCardBody>
    </StyledCard>
);