import styled from 'styled-components';
import asset from '../assets/sunlight.svg'

const StyledAboutMe = styled.span`
    display: flex;
    flex-direction: column;
    
    .title {
        color: var(--dark);
        width: 100%;
        border-bottom: var(--dark) solid 1px;

        margin-bottom: 0px;
    }
    
    gap: 20px;
`;

const StyledContent = styled.span`
    display: flex;
    flex-direction: row;

    span {
        height: 250px;
        display: flex;
        justify-content: center;
    }

    .paragraph {
        width: 66%;

        display: flex;
        flex-direction: column;

        text-align: right;

        p {
            max-width: 85%;
            margin-top: 0;
            margin-left: 25px;
        }
    }

    .asset-box {
        width: 33%;
        align-items: center;

        img {
            width: 90%;
            height: 90%;

            padding: 10px 0;
            border-radius: 200em;
            background: linear-gradient(
                to bottom, 
                var(--alternate), 
                transparent
            );
        }
    }
`

export const AboutMe: React.FC<any> = () => {
    return (
        <StyledAboutMe>
            <h2 className='title'>About me</h2>
            <StyledContent>
                <span className='paragraph'>
                    <p>I'm a passionate software developer based on Seville, Spain. Here you can have a small piece of personal info, like hobbies. For example, I used to train powerlifting and boxing.</p>
                    <p>In this other paragraph you can include some more info about what motivated you to learn to code. For example, to me it was that I was always a great fan of computers.</p>
                    <p>Now you can finish with some info about what motivates you to keep growing. To me it is I'm quality focused and I love to share knowledge with mates.</p>
                    <p>And now we have a block of text talking about us.</p>
                </span>
                <span className='asset-box'>
                    <img src={asset} alt='illustration asset' />
                </span>
            </StyledContent>
            <span></span>
        </StyledAboutMe>
    );
}