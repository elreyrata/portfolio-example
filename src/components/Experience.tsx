import React from 'react';
import styled from 'styled-components';
import { Card } from './Card';
import typescript from '../assets/typescript.png'
import csharp from '../assets/csharp.png'
import azure from '../assets/azure.png'

const StyledExperience = styled.span`
    display: flex;
    flex-direction: column;

    .title {
        color: var(--dark);
        width: 100%;
        border-bottom: var(--dark) solid 1px;

        margin-bottom: 0px;
    }

    gap: 20px;
`;



export const Experience: React.FC<any> = () => {

    return (
        <StyledExperience>
            <h2 className='title'>Experience</h2>
            <Card position='Full Stackoverflow' 
                company='Beginners' 
                description='In this position I was just stealing someother devs code.' 
                assets={[ typescript, csharp, azure ]}
            />
        </StyledExperience>
    );
}