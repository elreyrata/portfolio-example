import styled from 'styled-components';
import './App.css';
import { Header } from './components/Header';
import { Experience } from './components/Experience';
import { AboutMe } from './components/AboutMe';
import { Footer } from './components/Footer';

const StyledApp = styled.div`
  display: flex;
  flex-direction: column;

  min-height: 100vh;

  background-color: var(--bright);
`

const StyledContent = styled.span`
  width: 80%;
  display: flex;
  flex-direction: column;
  align-self: center;

  height: 60%;

  padding-bottom: 120px;
  max-width: 1560px;
`

function App() {
  return (
    <StyledApp>
      <Header />
      <StyledContent>
        <AboutMe />
        <Experience />
      </StyledContent>
      <Footer />
    </StyledApp>
  );
}

export default App;
